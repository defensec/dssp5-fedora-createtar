Summary: Bash script to create dssp5-fedora container archives
Name: dssp5-fedora-createtar
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Tools
Source: dssp5-fedora-createtar.tgz
URL: https://github.com/DefenSec/dssp5-fedora-createtar
Requires: dnf dssp5-fedora policycoreutils sudo systemd tar
BuildRequires: argbash make python3-docutils
BuildArch: noarch

%description
Bash script to create dssp5-fedora container archives.

%prep
%autosetup -n dssp5-fedora-createtar

%build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Mon Apr 26 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package
